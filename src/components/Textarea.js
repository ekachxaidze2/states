import "./Textarea.css"
import React, {useState} from "react";
const Textarea = () => {
    const [text, SetText] = useState("Hello woofer")
    const textChange = (event) => {
        SetText(event.target.value)
    }
    return(
     <div className="text__wrapper">
         <textarea onChange={textChange}>{text}</textarea>
         <button onClick={()=>console.log(text)}>Click me if u dare</button>
     </div>
    )
}

export default Textarea;
