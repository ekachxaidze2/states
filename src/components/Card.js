import React, {useState} from "react";
import "./Card.css"


const Card = ({image, title, level, selected}) => {
    const [cardSelected, setcardSelected] = useState(selected);
    const checkboxClicked = (event) => {
        setcardSelected(event.target.checked)
    }
    return(
        <div className={`rendom__card ${cardSelected ? "rendom__card--border" : ""}`}>
            <input type="checkbox" checked={cardSelected} onChange={checkboxClicked}/>
            <div className="card__img">
                <img src={image} alt="" />
            </div>
            <div className="card__title">{title}</div>
            <div className="card__level">{level}</div>
        </div>
    )
}
export default Card;
