import "./Button.css"
function Button ({
    label,
    handleClick
})  {
    return(
        <input className="counterBtn" type="button" onClick={handleClick} value={label}/>
    )
}
export default Button;
