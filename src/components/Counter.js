import Button from "./Button";
import React, {useState} from "react";
import "./Counter.css";
const Counter = () => {
    const [count, setCount] = useState(0);
    const increase = () => setCount(count + 1)
    const decrease = () => setCount(count - 1)
    return(
        <div className="counterCont">
            <Button label="+" handleClick={increase}/>
            {count}
            <Button label="-" handleClick={decrease}/>
        </div>
    )
}
export default Counter;
