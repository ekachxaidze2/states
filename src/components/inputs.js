import Counter from "./Counter";
import Textarea from "./Textarea";
import Card from "./Card";
import "./inputs.css"
function Inputs() {
    const doggos = [
        {
            image: "https://media1.popsugar-assets.com/files/thumbor/QKxxHUXEf7cYg0RV2CHLSF_2SpE/0x62:2000x2062/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2019/11/07/696/n/1922243/f9da03015dc43b5db45e80.98843139_/i/golden-retriever-is-scared-hair-straightener-video.jpg",
            title: "confused doggo",
            level: "level 2000"
        },
        {
            image: "https://www.dogsforgood.org/app/uploads/2019/06/Georgie-web.jpg",
            title: "good boy",
            level: "level 9000"
        },
        {
            image: "https://goldenhearts.co/wp-content/uploads/2019/02/oliver.jpg",
            title: "Small woofer",
            level: "level 10"
        }
    ]
return(
   <div className="main__wrapper">
       <Counter/>
       <Textarea/>
       <div className="catadog">
           {
               doggos.map(item => (
                   <Card image={item.image} title={item.title} level={item.level}/>
               ))
           }
       </div>
   </div>
)
}
export default Inputs;
